import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "./Pages/HomePage/HomePage";
import Layout from "./Pages/HOC/Layout/Layout";
import SignIn from "./Login/SignIn";
import SignUp from "./Login/SignUp";
import HomeTemplate from "templates/HomeTemplate/HomeTemplate";
import Home from "Pages/Home/Home";
import Detail from "Pages/Detail/Detail";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={
              <Layout>
                <HomePage />
                {/* <HomeTemplate /> */}
              </Layout>
            }
          />
          <Route path="/SignIn" element={<SignIn />} />
          <Route path="/SignUp" element={<SignUp />} />

          <Route path="detail/:id" element={<Detail />} />

          <Route path="" element={<HomeTemplate />}>
            <Route index path="" element={<Home />}></Route>
          </Route>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
