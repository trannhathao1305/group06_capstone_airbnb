import MenuMid from './../../Slider/MenuMid';
import { Menu } from 'antd';
import React from 'react'

function getItem(label, key, children, type) {
    return {
        key,
        children,
        label,
        type,
    };
}
const items = [
    getItem(<MenuMid />, 'sub1', [
        getItem('test'),
    ]),
];


export default function MidNav() {
    const onClick = (e) => {
        console.log('click ', e);
    };
    return (
        <Menu
            onClick={onClick}
            style={{
                width: 450,
            }}
            defaultSelectedKeys={['1']}
            defaultOpenKeys={['sub1']}
            mode="inline"
            items={items}
        />
    )
}





// import React, { Component } from 'react'


// export default class MidNav extends Component {
//     render() {
//         return (
//             <div>
//                 <menu className='flex flex-row rounded-full shadow-lg border bg-white px-4 py-3'>
//                     <div className="font-medium border-r pr-2 pl-2">Địa điểm bất kỳ</div>
//                     <div className="font-medium border-r pr-2 pl-2">Tuần bất kỳ</div>
//                     <div className="font-medium pr-2 pl-2">Thêm khách</div>
//                 </menu>
//             </div>
//         )
//     }
// }
