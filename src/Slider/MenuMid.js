import React, { Component } from 'react'

export default class MenuMid extends Component {
    render() {
        return (
            <menu className='flex flex-row rounded-full shadow-lg border bg-white px-4 py-3'>
                <div className="font-medium border-r pr-2 pl-2">Địa điểm bất kỳ</div>
                <div className="font-medium border-r pr-2 pl-2">Tuần bất kỳ</div>
                <div className="font-medium pr-2 pl-2">Thêm khách</div>
            </menu>
        )
    }
}