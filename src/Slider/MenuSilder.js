import React, { Component } from "react";
import Slider from "react-slick";

function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{
                ...style, display: "block", background: "black", text: "black",
            }}
            onClick={onClick}
        />
    );
}

function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
        <div
            className={className}
            style={{
                ...style, display: "block", background: "black", text: "black"
            }}
            onClick={onClick}
        />
    );
}

export default class CustomArrows extends Component {
    render() {
        const settings = {
            // dots: true,
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 1,
            nextArrow: <SampleNextArrow />,
            prevArrow: <SamplePrevArrow />
        };
        return (
            <div className="container mx-auto mt-10">
                <Slider {...settings}>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/c5a4f6fc-c92c-4ae8-87dd-57f1ff1b89a6.jpg" alt="" className="w-8 h-8 mx-auto mb-3 " />
                        <span className="font-sans">Thật ấn tượng</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/c0a24c04-ce1f-490c-833f-987613930eca.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Công viên quốc gia</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/3fb523a0-b622-4368-8142-b5e03df7549b.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Hồ bơi tuyệt vời</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/8e507f16-4943-4be9-b707-59bd38d56309.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Đảo</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/10ce1091-c854-40f3-a2fb-defc2995bcaf.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Bãi biển</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/35919456-df89-4024-ad50-5fcb7a472df9.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Nhà nhỏ</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/50861fca-582c-4bcc-89d3-857fb7ca6528.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Thiết kế</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/8b44f770-7156-4c7b-b4d3-d92549c8652f.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Bắc cực</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/732edad8-3ae0-49a8-a451-29a8010dcc0c.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Cabin</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/677a041d-7264-4c45-bb72-52bff21eb6e8.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Ven hồ</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/6b639c8d-cf9b-41fb-91a0-91af9d7677cc.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Chơi golf</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/3b1eb541-46d9-4bef-abc4-c37d77e3c21b.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Khung cảnh tuyệt vời</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/4221e293-4770-4ea8-a4fa-9972158d4004.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Hang động</span>
                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/957f8022-dfd7-426c-99fd-77ed792f6d7a.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Lướt sóng</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/1d477273-96d6-4819-9bda-9085f809dad3.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Khung nhà chữ A</span>

                    </div>
                    <div className="w-80 p-5 mr-2 text-center cursor-pointer hover:border-b-2 border-b-gray-500">
                        <img src="https://a0.muscache.com/pictures/d7445031-62c4-46d0-91c3-4f29f9790f7a.jpg" alt="" className="w-8 h-8 mx-auto mb-3" />
                        <span className="font-sans">Nhà dưới lòng đất</span>
                    </div>
                </Slider>
            </div>
        );
    }
}