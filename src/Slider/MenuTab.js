import { Dropdown, Space } from 'antd';
import { FaBars, FaUserCircle } from "react-icons/fa";
import React from 'react'
const items = [
    {
        label: <a href="/SignUp" className=''>Đăng ký</a>,
        key: '0',
    },
    {
        label: <a href="/SignIn">Đăng nhập</a>,
        key: '1',
    },
    {
        type: 'divider',
    },
    {
        label: 'Cho thuê nhà',
        key: '3',
    },
    {
        label: 'Tổ chức trải nghiệm',
        key: '4',
    },
    {
        label: 'Trợ giúp',
        key: '5',
    },
];
export default function MenuTab() {
    return (
        <Dropdown
            menu={{
                items,
            }}
            trigger={['click']}
        >
            <a onClick={(e) => e.preventDefault()}>
                <Space className='rounded-full border shadow-lg px-2 text-3xl py-1 cursor-pointer'>
                    <FaBars className='pl-3 mr-1' />
                    <FaUserCircle className='text-4xl' />
                </Space>
            </a>
        </Dropdown>
    )
}
