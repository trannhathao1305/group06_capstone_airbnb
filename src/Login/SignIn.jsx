import React from "react";
import { Button, Form, Input, message } from "antd";
import { postLogin } from "../Pages/Services/UserService";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { userLocalService } from "../Pages/Services/LocalService";
// import { setUserLogin } from '../Redux-toolkit/UserSlice';
import { SET_USER_LOGIN } from "./../Redux/constants/UserContants";

export default function SignIn() {
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const onFinish = (values) => {
    postLogin(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng nhập thành Công");
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        userLocalService.set(res.data.content);
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
        message.error("đăng nhập thất bại");
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div
      className="h-screen w-screen"
      style={{
        backgroundImage: `url("https://demo4.cybersoft.edu.vn/static/media/logo_login.a444f2681cc7b623ead2.jpg")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      <div div className="w-full p-5">
        <div className="absolute top-1/2 left-1/2 -translate-x-1/2 -translate-y-1/2 border rounded-lg shadow-lg bg-white px-10 py-5 w-4/5 sm:w-1/2 md:w-2/5">
          <div className="font-semibold text-3xl text-blue-800 text-center">
            Đăng nhập
          </div>
          <div>
            <Form
              className="text-center w-full"
              layout="vertical"
              name="basic"
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 24 }}
              initialValues={{ remember: true }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
            >
              <Form.Item
                label="Email"
                name="email"
                rules={[
                  { required: true, message: "Please input your username!" },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Password"
                name="password"
                rules={[
                  { required: false, message: "Please input your password!" },
                ]}
              >
                <Input.Password />
              </Form.Item>
              <div wrapperCol={{ span: 16 }} className="text-center">
                <div className="flex justify-between items-center mb-6">
                  <div className="text-rose-700 hover:text-rose-500 hover:underline underline-offset-4 tracking-wider duration-200 active">
                    Quên mật khẩu ?
                  </div>
                  <div>
                    <Button
                      htmlType="submit"
                      className="text-white focus:outline-none focus:ring-4 px-20 font-medium rounded-lg text-sm text-center bg-red-500 hover:bg-red-800 duration-300"
                    >
                      Đăng nhập
                    </Button>
                  </div>
                </div>
              </div>
            </Form>
          </div>
          <div className="text-center">
            <p>
              Chưa có tài khoản{" "}
              <a
                class="text-rose-700 hover:text-rose-500 hover:underline underline-offset-4 tracking-wider duration-200"
                href="/register"
              >
                Đăng ký ngay
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
