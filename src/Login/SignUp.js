import React from "react";
import { postRegister } from "../Pages/Services/UserService";
import { useNavigate } from "react-router-dom";

import { DatePicker, Form, Input, message, Select } from "antd";
const { Option } = Select;

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 24,
    },
  },
  wrapperCol: {
    xs: {
      span: 32,
    },
    sm: {
      span: 24,
    },
  },
};

export default function SingUp() {
  // let dispatch = useDispatch();
  let navigate = useNavigate();
  const onFinish = (values) => {
    console.log("values: ", values);
    postRegister(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng ký thành Công");
        navigate("/");
      })
      .catch((err) => {
        console.log(err);
        message.success("Đăng ký thất bại");
      });
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  // const [form] = useForm();

  // const prefixSelector = (
  //     <Form.Item name="prefix" noStyle>
  //         <Select
  //             style={{
  //                 width: 70,
  //             }}
  //         >
  //             <Option value="84">+84</Option>
  //         </Select>
  //     </Form.Item>
  // );

  return (
    <div
      className="h-screen w-screen"
      style={{
        backgroundImage: `url("https://demo4.cybersoft.edu.vn/static/media/logo_login.a444f2681cc7b623ead2.jpg")`,
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
      }}
    >
      <div className="md:absolute top-1/2 left-1/2 md:-translate-x-1/2 md:-translate-y-1/2 border rounded-lg shadow-lg bg-white px-10 w-4/5 sm:w-2/3 mx-auto">
        <div class="mb-4 relative">
          <div class="hidden md:block text-center font-semibold text-3xl text-blue-800">
            <h1>Đăng ký tài khoản</h1>
          </div>
        </div>
        <Form
          className="grid grid-cols-2 gap-x-3"
          {...formItemLayout}
          layout="vertical"
          // form={form}
          name="register"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          // scrollToFirstError
        >
          {/* Tên người dung */}
          <Form.Item
            name="name"
            label="Tên Người Dùng"
            rules={[
              {
                required: true,
                message: "Please input your nickname!",
                whitespace: true,
              },
            ]}
          >
            <Input
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Điền tên vào đây ..."
            />
          </Form.Item>
          {/* Email */}
          <Form.Item
            name="email"
            label="Email"
            rules={[
              {
                type: "email",
                message: "The input is not valid E-mail!",
              },
              {
                required: true,
                message: "Please input your E-mail!",
              },
            ]}
          >
            <Input
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Điền email vào đây ..."
            />
          </Form.Item>
          {/* Số điện thoại */}
          <Form.Item
            name="phone"
            label="Số Điện Thoại"
            rules={[
              {
                required: true,
                message: "Please input your phone number!",
              },
            ]}
          >
            <Input
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Điền số điện thoại vào đây ..."
            />
          </Form.Item>
          {/* Mật khẩu */}
          <Form.Item
            name="password"
            label="Mật Khẩu"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Điền mật khẩu vào đây ..."
            />
          </Form.Item>
          {/* Địa chỉ */}
          <Form.Item
            name="role"
            label="Địa chỉ"
            rules={[
              {
                required: true,
                message: "",
                whitespace: true,
              },
            ]}
          >
            <Input
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Điền địa chỉ vào đây ..."
            />
          </Form.Item>
          {/* Ngày sinh */}
          <Form.Item name="birthday" label="Ngày Sinh" validateStatus="success">
            <DatePicker className=" border text-white text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-white dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" />
          </Form.Item>
          {/* Giới tính */}
          <Form.Item
            name="gender"
            label="Select"
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please select your country!",
              },
            ]}
          >
            <Select>
              <Option value="Nam">Nam</Option>
              <Option value="Nữ">Nữ</Option>
            </Select>
          </Form.Item>
          <Form.Item></Form.Item>
          <div className="col-span-2">
            <div class="col-span-2 text-center">
              <button
                htmlType="submit"
                class="text-white focus:outline-none focus:ring-4  font-medium rounded-full text-sm px-5 py-2.5 text-center mr-2 mb-2 bg-red-500 hover:bg-red-800 duration-300 w-1/2"
              >
                Đăng ký
              </button>
            </div>
            <a
              class="col-span-2 text-rose-600 hover:text-rose-500 hover:underline underline-offset-4 tracking-wider duration-200 flex justify-center"
              href="/SignIn"
            >
              Đăng nhập ngay
            </a>
          </div>
        </Form>
      </div>
    </div>
  );
}
