import { userLocalService } from "../Pages/Services/LocalService";
import { SET_USER_LOGIN } from './constants/UserContants';

const initialState = {
    user: userLocalService.get(),
}

export const UserReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        case SET_USER_LOGIN:
            return { ...state, user: payload };

        default:
            return state
    }
}
