import { combineReducers } from "redux";
import { UserReducer } from './UserReducer';
import { Auth } from "./reducers/AuthReducer";
import { CommentsReducer } from "./reducers/CommentsReducer";
import { LocationReducer } from "./reducers/LocationReducer";
import { RoomReducers } from "./reducers/RoomReducer";
import { BookRoomReducer } from "./reducers/BookRoomReducer";

export const RootReducer = combineReducers({ UserReducer, Auth,
    CommentsReducer,
    LocationReducer,
    RoomReducers,
    BookRoomReducer});