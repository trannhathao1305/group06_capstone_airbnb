
import { https } from './ConfigUrl';
export const postLogin = (data) => {
    return https.post("/api/auth/signin", data);
}
export const postRegister = (data) => {
    return https.post("/api/auth/signup", data);
}