import React, { Component } from 'react'
import MenuSilder from './../../Slider/MenuSilder';

export default class HomePage extends Component {
    render() {
        return (
            <div>
                <MenuSilder />
            </div>
        )
    }
}
